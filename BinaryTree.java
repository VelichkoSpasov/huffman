package huffman;

import java.util.HashMap;

public class BinaryTree {
	public static int treeHeight;
	public static Node LastNode;
	public static String wholeCode = "";
	public static HashMap<Character, Node> charCodes = new HashMap<>();

	public static void PrintNode(int n, int h, Node node) {
		for (int i = 0; i < h; i++)
			System.out.print("\t");
		if (node != null)
			System.out.print(n + node.getCharacter() + "\n");
		else
			System.out.print(n + "\n");

		if (treeHeight < h)
			treeHeight = h;
	}

	public static void Preorder(Node node, int height) {
		if (node == null) {
			PrintNode(0, height, null);

			return;
		}

		Preorder(node.getLeft(), height + 1);
		PrintNode(node.getValue(), height, node);
		Preorder(node.getRight(), height + 1);
	}

	public static void myTree(Node startNode, int zero_one, String oldStr) {
		String str = oldStr;

		if (zero_one == 0) {
			str += zero_one;
		}
		else if (zero_one == 1) {
			str += zero_one;
		}
		
		if (startNode.getLeft() == null && startNode.getRight() == null) {
			startNode.setCode(str);
			System.out.println(startNode + " code: " + startNode.getCode());
			charCodes.put(startNode.getCharacter(), startNode);

			return;
		}

		myTree(startNode.getLeft(), 0, str);
		myTree(startNode.getRight(), 1, str);
	}
}
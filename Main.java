package huffman;

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

public class Main {

	public static void main(String[] args) {
		String text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, "
				+ "sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. "
				+ "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris "
				+ "nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in "
				+ "reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla "
				+ "pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa "
				+ "qui officia deserunt mollit anim id est laborum.";

		PriorityQueue<Node> numbers = new PriorityQueue<>();
		Node node = null;
		Node LastNode = null;
		HashMap<Character, Integer> countMap = new HashMap<>();

		for (char c : text.toCharArray()) {
			if (countMap.containsKey(c)) {
				countMap.put(c, countMap.get(c) + 1);
			} else {
				countMap.put(c, 1);
			}
		}

		for (Map.Entry<Character, Integer> mapEle : countMap.entrySet()) {
			System.out.println(mapEle.getKey() + " " + mapEle.getValue());

			node = new Node(mapEle.getKey(), mapEle.getValue());
			numbers.add(node);
		}

		System.out.println("-------------------------------------");

		while (!numbers.isEmpty()) {
			Node tempEle1 = numbers.poll();

			if (numbers.isEmpty()) {
				LastNode = tempEle1;
				BinaryTree.LastNode = LastNode;

				System.out.println(LastNode.toString());

				break;
			}

			Node tempEle2 = numbers.poll();
			Node newNode = new Node(' ',
					tempEle1.getValue() + tempEle2.getValue());

			newNode.setLeft(tempEle1);
			newNode.setRight(tempEle2);
			numbers.add(newNode);
			LastNode = newNode;
		}

		System.out.println("-------------------------------------");

		BinaryTree.Preorder(LastNode, 0);

		System.out.println("Height of the tree is: " + BinaryTree.treeHeight);

		BinaryTree.myTree(BinaryTree.LastNode, 2, "");
		
		for(int i = 0; i < text.length(); i++) {
			if(BinaryTree.charCodes.containsKey(text.charAt(i))) {
				BinaryTree.wholeCode += BinaryTree.charCodes.get(text.charAt(i)).getCode();
			}
		}
		
		System.out.println(BinaryTree.wholeCode);
		System.out.println(BinaryTree.wholeCode.length());
	}

}

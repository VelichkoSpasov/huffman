package huffman;

public class Node implements Comparable<Node> {
	private int value;
	private char character;
	private Node left;
	private Node right;
	private String code;

	Node(char ch, int val) {
		value = val;
		character = ch;
		left = null;
		right = null;
		code = "";
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public char getCharacter() {
		return character;
	}

	public void setCharacter(char character) {
		this.character = character;
	}

	public Node getLeft() {
		return left;
	}

	public void setLeft(Node left) {
		this.left = left;
	}

	public Node getRight() {
		return right;
	}

	public void setRight(Node right) {
		this.right = right;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public String toString() {
		return "Node [value=" + value + ", character=" + character + "]";
	}

	@Override
	public int compareTo(Node o) {
		if (this.value > o.value)
			return 1;
		else if (this.value < o.value)
			return -1;
		else
			return 0;
	}/*
		 * 
		 * @Override public int hashCode() { final int prime = 31; int result = 1;
		 * result = prime * result + value; return result; }
		 * 
		 * @Override public boolean equals(Object obj) { if (this == obj) return true;
		 * if (obj == null) return false; if (getClass() != obj.getClass()) return
		 * false; Node other = (Node) obj; if (value != other.value) return false;
		 * return true; }
		 */
}
